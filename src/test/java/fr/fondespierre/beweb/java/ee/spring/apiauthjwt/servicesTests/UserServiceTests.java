package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.servicesTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.repositories.UserRepository;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.UserService;

@RunWith(SpringRunner.class)
public class UserServiceTests {
	
	@TestConfiguration
	static class UserServiceTestsContextConfiguration {
		
		@Bean
		public UserService userService() {
			return new UserService();
		}
	}
	
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepository userRepository;
	
	
	
	
	/* Tests on identifyUser(String email, String password) 
	 * The expected return is of type User
	 * Exceptions must be thrown in cases or error:
	 * - BadCredentialsException,
	 * - NullParametersException, 
	 * - EmptyParametersException,
	 * - DatabaseServerException */
	
	@Test
	public void whenIdentifyUser_thenUserShouldBeFound() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("gali@net");
		user.setProfile(profile);
		user.setPassword("sixoeufs");
		
		try {
			BDDMockito.given(this.userRepository.findByEmailAndPassword(user.getProfile().getEmail(), user.getPassword())).willReturn(user);
			User found = null;
			try {
				found = this.userService.identifyUser(user.getProfile().getEmail(), user.getPassword());
				Assert.assertEquals(found, user);
			} catch (NullParametersException e) {
				e.printStackTrace();
			} catch (EmptyParametersException e) {
				e.printStackTrace();
			} catch (BadCredentialsException e) {
				e.printStackTrace();
			} catch (DatabaseServerException e) {
				e.printStackTrace();
			}
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test(expected = BadCredentialsException.class)
	public void whenBadCredentialsExceptionIsThrown_thenExpectationSatisfied() throws BadCredentialsException {
		String email = "gali@net";
		String password = "sixoeufs";
		
		try {
			BDDMockito.given(this.userRepository.findByEmailAndPassword(email, password)).willReturn(null);
			try {
				this.userService.identifyUser(email, password);
			} catch (NullParametersException e) {
				e.printStackTrace();
			} catch (EmptyParametersException e) {
				e.printStackTrace();
			} catch (BadCredentialsException e) {
				e.printStackTrace();
				throw new BadCredentialsException();
			} catch (DatabaseServerException e) {
				e.printStackTrace();
			}
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test(expected = NullParametersException.class)
	public void WhenNullParametersExceptionIsThrown_thenExpectationSatisfied() throws NullParametersException {
		String email = null;
		String password = null;
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullParametersException.class)
	public void WhenNullParametersExceptionIsThrown_thenExpectationSatisfied_2() throws NullParametersException {
		String email = null;
		String password = "";
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullParametersException.class)
	public void WhenNullParametersExceptionIsThrown_thenExpectationSatisfied_3() throws NullParametersException {
		String email = "";
		String password = null;
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullParametersException.class)
	public void WhenNullParametersExceptionIsThrown_thenExpectationSatisfied_4() throws NullParametersException {
		String email = "jany";
		String password = null;
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullParametersException.class)
	public void WhenNullParametersExceptionIsThrown_thenExpectationSatisfied_5() throws NullParametersException {
		String email = null;
		String password = "versace";
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = EmptyParametersException.class)
	public void WhenEmptyParametersExceptionIsThrown_thenExpectationSatisfied() throws EmptyParametersException {
		String email = "";
		String password = "";
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
			throw new EmptyParametersException();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = EmptyParametersException.class)
	public void WhenEmptyParametersExceptionIsThrown_thenExpectationSatisfied_2() throws EmptyParametersException {
		String email = "";
		String password = "versace";
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
			throw new EmptyParametersException();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = EmptyParametersException.class)
	public void WhenEmptyParametersExceptionIsThrown_thenExpectationSatisfied_3() throws EmptyParametersException {
		String email = "jany";
		String password = "";
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
			throw new EmptyParametersException();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = DatabaseServerException.class)
	public void WhenDataBaseServerExceptionIsThrown_thenExpectationSatisfied() throws DatabaseServerException {
		String email = "gali@net";
		String password = "sixoeufs";
		
		try {
			BDDMockito.given(this.userRepository.findByEmailAndPassword(email, password)).willThrow(DatabaseServerException.class);
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
		
		try {
			this.userService.identifyUser(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
			throw new DatabaseServerException();
		}
	}
	
}
