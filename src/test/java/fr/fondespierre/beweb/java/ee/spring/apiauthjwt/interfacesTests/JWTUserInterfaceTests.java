package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfacesTests;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;

@RunWith(SpringRunner.class)
public class JWTUserInterfaceTests {
	
	@Test
	public void getUserEmailTest() {
		String email = "gali@net";
		String wrongEmail = "gali";
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail(email);
		user.setProfile(profile);
		String returned = user.getUserEmail();
		Assert.assertEquals(email, returned);
		Assert.assertNotEquals(wrongEmail, returned);
		Assert.assertNotNull(returned);
	}
	
	@Test
	public void setUserEmailTest() {
		String email = "gali@net";
		User user = new User();
		Profile profile = new Profile();
		user.setProfile(profile);
		user.setUserEmail(email);
		Assert.assertEquals(email, user.getProfile().getEmail());
		Assert.assertEquals(email, user.getUserEmail());
		Assert.assertNotNull(user.getProfile().getEmail());
		Assert.assertNotNull(user.getUserEmail());
	}
	
	@Test
	public void getSerializedTest() {
		String email = "gali@net";
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail(email);
		user.setProfile(profile);
		Map<String, ?> returned = user.getSerialized();
		Assert.assertNotNull(returned);
		Assert.assertEquals(email, returned.get("email"));
	}

}
