package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.servicesTests;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Authorization;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.AuthService;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.JWTService;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.UserService;

@RunWith(SpringRunner.class)
public class AuthServiceTests {
	
	@TestConfiguration
	static class AuthServiceTestsContextConfiguration {
		
		@Bean
		public AuthService authService() {
			return new AuthService();
		}
		
	}
	
	@Autowired
	private AuthService authService;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private JWTService jwtService;
	
	/* Tests on isAuthorized(String email, String password) 
	 * The expected return is of type Authorization
	 * Exceptions must be thrown in cases or error:
	 * - BadCredentialsException,
	 * - NullParametersException, 
	 * - EmptyParametersException,
	 * - DatabaseServerException */
	
	@Test
	public void whenIsAuthorized_thenAuthorizationShouldBeReturned() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("gali@net");
		user.setProfile(profile);
		user.setPassword("sixoeufs");
		
		try {
			BDDMockito.given(this.userService.identifyUser(user.getProfile().getEmail(), user.getPassword())).willReturn(user);
			Authorization authorization = new Authorization();
			authorization.setProfile(user.getProfile());
			Authorization returned = null;
			try {
				returned = this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword());
				Assert.assertEquals(returned.getProfile(), authorization.getProfile());
			} catch (NullParametersException e) {
				e.printStackTrace();
			} catch (EmptyParametersException e) {
				e.printStackTrace();
			} catch (BadCredentialsException e) {
				e.printStackTrace();
			} catch (DatabaseServerException e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = BadCredentialsException.class)
	public void whenBadCredentialsExceptionIsThrown_thenExpectationSatisfied() throws BadCredentialsException {
		String email = "gali@net";
		String password = "sixoeufs";
		try {
			BDDMockito.given(this.userService.identifyUser(email, password)).willThrow(BadCredentialsException.class);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
		
		try {
			this.authService.isAuthorized(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
			throw new BadCredentialsException();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = DatabaseServerException.class)
	public void whenDatabaseServerExceptionIsThrown_thenExpectationSatisfied() throws DatabaseServerException {
		String email = "gali@net";
		String password = "sixoeufs";
		
		try {
			BDDMockito.given(this.userService.identifyUser(email, password)).willThrow(DatabaseServerException.class);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
		
		try {
			this.authService.isAuthorized(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
			throw new DatabaseServerException();
		}
	}
	
	@Test(expected = NullParametersException.class)
	public void whenNullParametersExceptionIsThrown_thenExpectationSatisfied() throws NullParametersException {
		String email = null;
		String password = null;
		
		try {
			BDDMockito.given(this.userService.identifyUser(email, password)).willThrow(NullParametersException.class);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
		
		try {
			this.authService.isAuthorized(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = EmptyParametersException.class)
	public void whenEmptyParametersIsThrown_thenExpectationSatisfied() throws EmptyParametersException {
		String email = "";
		String password ="";
		
		try {
			BDDMockito.given(this.userService.identifyUser(email, password)).willThrow(EmptyParametersException.class);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
		
		try {
			this.authService.isAuthorized(email, password);
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
			throw new EmptyParametersException();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	

}
