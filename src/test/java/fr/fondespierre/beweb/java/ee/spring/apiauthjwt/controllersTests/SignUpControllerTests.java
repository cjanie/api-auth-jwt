package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllersTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllers.SignUpController;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(SignUpController.class)
public class SignUpControllerTests {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean // refers to @Autowired in the tested class
	private UserService userService;
	
	private final String uri = "/rest/sign-up";
	
	@Test
	public void createUserTest() {
		User user = new User();
		user.setId(1L);
		Profile profile = new Profile();
		user.setProfile(profile);
		try {
			BDDMockito.given(this.userService.add(user)).willReturn(user.getId());
			try {
				this.mockMvc.perform(
						MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsString(user)) // Convert the JAVA object to a JSON string with the Jackson Dependency
						)
				.andExpect(MockMvcResultMatchers.status().isCreated());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
}
