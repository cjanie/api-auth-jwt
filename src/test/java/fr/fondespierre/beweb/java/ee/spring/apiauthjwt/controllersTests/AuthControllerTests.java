package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllersTests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllers.AuthController;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Authorization;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.AuthService;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthController.class)
public class AuthControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean // Reference to @Autowired in the tested class
	private AuthService authService;
	
	private final String uri = "/rest/auth";
	
	
	/*
	 * Test authentication(User user)
	 * The http protocol is POST method to the url "/rest/auth"
	 * The expected return is of type ResponseEntity with different status code:
	 * - OK
	 * - INTERNAL_SERVER_ERROR
	 * - UNAUTHORIZED
	 * - NOT_ACCEPTABLE
	 * - I_AM_A_TEA_POT
	 * */
	
	@Test
	public void AuthenticationIsOK() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("tape");
		user.setProfile(profile);
		user.setPassword("nade");
		
		try {
			BDDMockito.given(this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword())).willReturn(new Authorization());
			try {
				this.mockMvc.perform(
						MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsString(user)) // Convert the JAVA object to a JSON string with the Jackson Dependency
						)
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void AuthenticationEncountersAnInternalServerError() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("tape");
		user.setProfile(profile);
		user.setPassword("nade");
		
		try {
			BDDMockito.given(this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword())).willThrow(DatabaseServerException.class);
			try {
				this.mockMvc.perform(MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsBytes(user))
						)
				.andExpect(MockMvcResultMatchers.status().isInternalServerError());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void authenticationWithBadCredentialsIsUnauthorized() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("tape");
		user.setProfile(profile);
		user.setPassword("nade");
		
		try {
			BDDMockito.given(this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword())).willThrow(BadCredentialsException.class);
			try {
				this.mockMvc.perform(MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsString(user))
						)
				.andExpect(MockMvcResultMatchers.status().isUnauthorized());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void authenticationWithNullParametersIsNotAcceptable() {
		User user = new User();
		Profile profile = new Profile();
		user.setProfile(profile);
		
		try {
			BDDMockito.given(this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword())).willThrow(NullParametersException.class);
			try {
				this.mockMvc.perform(MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsString(user))
						).andExpect(MockMvcResultMatchers.status().isNotAcceptable());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void authenticationWithEmptyParametersIsATeapot() {
		User user = new User();
		Profile profile = new Profile();
		profile.setEmail("");
		user.setProfile(profile);
		user.setPassword("");
		
		try {
			BDDMockito.given(this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword())).willThrow(EmptyParametersException.class);
			try {
				this.mockMvc.perform(MockMvcRequestBuilders.post(this.uri)
						.contentType(MediaType.APPLICATION_JSON)
						.content((new ObjectMapper()).writeValueAsString(user))
						)
				.andExpect(MockMvcResultMatchers.status().isIAmATeapot());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (NullParametersException e) {
			e.printStackTrace();
		} catch (EmptyParametersException e) {
			e.printStackTrace();
		} catch (BadCredentialsException e) {
			e.printStackTrace();
		} catch (DatabaseServerException e) {
			e.printStackTrace();
		}
	}
	

}
