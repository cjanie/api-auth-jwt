package fr.fondespierre.beweb.java.ee.spring.apiauthjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiauthjwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiauthjwtApplication.class, args);
	}
	
	

}
