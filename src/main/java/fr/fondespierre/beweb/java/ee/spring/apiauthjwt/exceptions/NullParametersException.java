package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions;

public class NullParametersException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NullParametersException() {
		super("Email and Password parameters can't be null" );
	}

}
