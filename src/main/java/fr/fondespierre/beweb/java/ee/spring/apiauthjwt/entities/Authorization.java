package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities;

public class Authorization {
	
	private Profile profile;
	
	private String token;

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	

}
