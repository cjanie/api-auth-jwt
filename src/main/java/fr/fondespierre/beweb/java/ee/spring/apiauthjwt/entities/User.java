package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfaces.JWTUserInterface;

@Entity
public class User implements JWTUserInterface {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private Profile profile;
	
	private String password;
	

	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Profile getProfile() {
		return profile;
	}


	public void setProfile(Profile profile) {
		this.profile = profile;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String getUserEmail() {
		return this.profile.getEmail();
	}


	@Override
	public void setUserEmail(String email) {
		this.profile = new Profile();
		this.profile.setEmail(email);
	}

	@Override
	public Map<String, ?> getSerialized() {
		Map<String, String> map = new HashMap<>();
		//map.put("firstName", this.profile.getFirstName());
		//map.put("lastName", this.profile.getLastname());
		map.put("email", this.profile.getEmail());
		return map;
	}

}
