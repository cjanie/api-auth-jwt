package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions;

public class BadCredentialsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadCredentialsException() {
		super("Incorrect combinaison of email and password");
	}
}
