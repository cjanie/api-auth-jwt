package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfaces;

import java.util.Map;

public interface JWTUserInterface {
	
	public String getUserEmail();
	
	public void setUserEmail(String email);
	
	public Map<String, ?> getSerialized();

}
