package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfaces.JWTUserInterface;

@Service
public class JWTService {
	
	/** 
	 * This service offers two functions: CREATE and VERIFY a JWT token
	 * 
	 * */
	
	/** 
	 * A SECRET PHRASE is required to create and verify a JWT token
	 * The phrase should be very long and complicate
	 * */
	Algorithm algorithm = Algorithm.HMAC256("jw.t nfr.t pw n(y).t tA nTr.t aA.t wr.t r pr-anx n(y) Jwnw m TA-mry");
	
	/**
	 * The function that creates the token
	 * @param user
	 * @return token as string
	 */
	public String createToken(JWTUserInterface user) {
		
		/* Define the date of expiration of the token **/
		Calendar calendar = Calendar.getInstance(); // Actual date
		calendar.add(2, 1); // Add 1 month
		Date expire = calendar.getTime();
		
		/* Create the token **/
		String token = JWT.create()
				//.withClaim("email", user.getUserEmail())
				.withClaim("user", user.getSerialized().toString()) // The good practice
				.withExpiresAt(expire)
				.sign(this.algorithm);
		return token;
	}
	/**
	 * The function that verifies the token
	 * @param token as string
	 * @return decoded token claims as string
	 */
	public String verifyToken (String token) {
		JWTVerifier verifier = JWT.require(this.algorithm).build();
		DecodedJWT jwt = verifier.verify(token);
		return "User: " + jwt.getClaim("user").asString() + " Token expiration date: " + jwt.getExpiresAt();
	}
	

}
