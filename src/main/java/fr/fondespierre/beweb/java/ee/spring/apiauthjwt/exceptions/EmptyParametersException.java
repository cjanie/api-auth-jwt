package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions;

public class EmptyParametersException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EmptyParametersException() {
		super("Email and Password parameters can't be empty.");
	}
}
