package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions;

public class DatabaseServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DatabaseServerException() {
		super("Database server error");
	}

}
