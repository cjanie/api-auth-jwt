package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Authorization;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfaces.JWTUserInterface;

@Service
public class AuthService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private JWTService jwtService;
	
	public Authorization isAuthorized(String email, String password) 
			throws NullParametersException, EmptyParametersException, 
			BadCredentialsException, 
			DatabaseServerException {
		try {
			User authUser = this.userService.identifyUser(email, password);
			Authorization authorization = new Authorization();
			authorization.setProfile(authUser.getProfile());
			JWTUserInterface user = new User();
			user = authUser;
			String token = this.jwtService.createToken(user);
			authorization.setToken(token);
			return authorization;
		} catch (NullParametersException e) {
			throw new NullParametersException();
		} catch (EmptyParametersException e) {
			throw new EmptyParametersException();
		} catch (BadCredentialsException e) {
			throw new BadCredentialsException();
		} catch (DatabaseServerException e) {
			throw new DatabaseServerException();
		}
	}

}
