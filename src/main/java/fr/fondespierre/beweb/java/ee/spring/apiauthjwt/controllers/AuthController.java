package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Authorization;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.Profile;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.AuthService;

@RestController
@RequestMapping(value = "/rest/auth")
@CrossOrigin(origins = "*")
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	
	@PostMapping
	public ResponseEntity<?> authentication(@RequestBody User user) {
		Authorization authorization = null;
		try {
			try {
				authorization = this.authService.isAuthorized(user.getProfile().getEmail(), user.getPassword());
			} catch (DatabaseServerException e) {
				return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (BadCredentialsException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.UNAUTHORIZED);
		} catch (NullParametersException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (EmptyParametersException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.I_AM_A_TEAPOT);
		}
		
		Profile profile = new Profile();
		profile = authorization.getProfile();
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("token", authorization.getToken());
		
		return new ResponseEntity<Profile>(profile, headers, HttpStatus.OK);
	}

}
