package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.interfaces.JWTUserInterface;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.JWTService;

@RestController
@RequestMapping(value = "/rest/auth")
@CrossOrigin(origins="*")
public class JWTController {
	
	/**
	 * Two GET requests: to create token and to verify token
	 * */
	
	@Autowired
	private JWTService jwtService;
	
	
	
	/**
	 * Request to create token
	 * @RequestParam String email, String password :
	 * http://localhost:2222/rest/auth/token?email=j@a
	 * */
	@GetMapping("/token")
	public ResponseEntity<String> getToken(@RequestParam String email) {
		JWTUserInterface user = new User();
		user.setUserEmail(email);
		return new ResponseEntity<String>(this.jwtService.createToken(user), HttpStatus.OK);
		
	}
	/*
	 * Request to verify token
	 * @RequestParam String token:
	 * http://localhost:2222/rest/auth/verify?token=eyJO....
	 */
	@GetMapping("/verify")
	public ResponseEntity<Void> verifyToken(@RequestParam String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("token-payload", this.jwtService.verifyToken(token));
		return new ResponseEntity<Void>(headers, HttpStatus.OK);
	}
}
