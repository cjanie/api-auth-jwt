package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.repositories;



import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;

public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findByEmailAndPassword(String email, String password) throws DatabaseServerException;

}
