package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services.UserService;

@RestController
@RequestMapping("rest/sign-up")
@CrossOrigin(origins = "*")
public class SignUpController {
	
	@Autowired
	UserService userService;
	
	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody User user) {
		Long id = 0L;
		try {
			id = this.userService.add(user);
		} catch (DatabaseServerException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody User user) {
		try {
			this.userService.update(user);
		} catch (DatabaseServerException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id) {
		try {
			this.userService.delete(id);
		} catch (DatabaseServerException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
	}

}
