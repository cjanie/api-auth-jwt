package fr.fondespierre.beweb.java.ee.spring.apiauthjwt.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.entities.User;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.BadCredentialsException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.DatabaseServerException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.EmptyParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.exceptions.NullParametersException;
import fr.fondespierre.beweb.java.ee.spring.apiauthjwt.repositories.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User identifyUser(String email, String password) 
			throws NullParametersException, EmptyParametersException, 
			BadCredentialsException, 
			DatabaseServerException {
		if(email == null || password == null) {
			throw new NullParametersException();
		} else if(email.isEmpty() || password.isEmpty()) {
			throw new EmptyParametersException();
		} else {
			try {
				User user = this.userRepository.findByEmailAndPassword(email, password);
				if(user == null) {
					throw new BadCredentialsException();
				} else {
					return user;
				}
			} catch (DatabaseServerException e) {
				throw new DatabaseServerException();
			}
		}
	}
	
	public Long add(User user) throws DatabaseServerException {
		return this.userRepository.save(user).getId();
	}
	
	public void update(User user) throws DatabaseServerException {
		this.userRepository.save(user);
	}
	
	public void delete(Long id) throws DatabaseServerException {
		this.userRepository.deleteById(id);
	}
}
